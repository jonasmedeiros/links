import React, { Component } from 'react';
import { Text, View} from "react-native";
import RNLanguages from 'react-native-languages';
import i18n from './i18n';
import { createStore, applyMiddleware } from 'redux';
import { Provider as StoreProvider  } from 'react-redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

import { createStackNavigator, createAppContainer } from "react-navigation";

import Root from './Root';
import RepoDetail from './RepoDetail';
import Profile from './Profile';

import allReducers from './reducers/index';

import { Provider as PaperProvider } from 'react-native-paper';

const client = axios.create({
  baseURL: 'https://api.github.com',
  responseType: 'json'
});

const store = createStore(allReducers, applyMiddleware(axiosMiddleware(client)));

const AppNavigator = createStackNavigator({
  home: Root,
  detail: RepoDetail,
});

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {

  componentWillMount() {
    RNLanguages.addEventListener('change', this._onLanguagesChange);
  }

  componentWillUnmount() {
    RNLanguages.removeEventListener('change', this._onLanguagesChange);
  }

  _onLanguagesChange = ({ language }) => {
    i18n.locale = language;
  };
  
  render() {
    return (
      <StoreProvider store={store}>
        <PaperProvider>
          <AppContainer />
        </PaperProvider>
      </StoreProvider>
    );
  }
}
