import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from "react-native";
import i18n from "./i18n";
import { connect } from 'react-redux';
import { listRepos } from './actions/repository.action';
import SplashScreen from 'react-native-splash-screen';
import { TextInput } from 'react-native-paper';
import { reduxForm, Field } from 'redux-form';
import validation from "./validation";
import MyTextInput from './MyTextInput'

class Root extends Component {
  state = {
    text: ''
  };

  static navigationOptions = {
    title: 'Repositories'
  };
  
  componentDidMount() {
    this.props.listRepos('relferreira');
    SplashScreen.hide();
  }

  renderItem = ({ item }) => (
    <TouchableOpacity
      style={styles.item}
      onPress={() =>
        this.props.navigation.navigate('detail', { name: item.name })
      }
    >
      <Text>{item.name}</Text>
    </TouchableOpacity>
  );

  render() {
    const { repos } = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.title}>{i18n.t("title")}</Text>

        <Text style={styles.line}>
          {i18n.t("current", { language: i18n.currentLocale() })}
        </Text>

        <Field
          name={'email'}
          component={MyTextInput}
        />

        <FlatList
          styles={styles.ListContainer}
          data={repos}
          renderItem={this.renderItem}
          keyExtractor={(repos) => repos.id.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF",
    paddingHorizontal: 10,
    paddingVertical: 30
  },
  title: {
    fontSize: 20,
    marginBottom: 10
  },
  line: {
    color: "#333333",
    marginBottom: 5
  },
  ListContainer: {
    flex: 1
  },
  item: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  }
});

const mapStateToProps = state => {
  let storedRepositories = state.repository.repos.map(repo => ({ key: repo.id, ...repo }));
  return {
    repos: storedRepositories
  };
};

const mapDispatchToProps = {
  listRepos
};

export default connect(mapStateToProps, mapDispatchToProps)(
  reduxForm({
    form: "test",
    validate: validation
  })(Root)
);