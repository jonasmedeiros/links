import { GET_REPO_INFO, GET_REPO_INFO_SUCCESS, GET_REPO_INFO_FAIL } from "../types/repository-detail.type";

export function getRepoDetail(user, repo) {
    return {
      type: GET_REPO_INFO,
      payload: {
        request: {
          url: `/repos/${user}/${repo}`
        }
      }
    };
}