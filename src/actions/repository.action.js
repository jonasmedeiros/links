import { GET_REPOS, GET_REPOS_SUCCESS, GET_REPOS_FAIL } from "../types/respository.type";

export function listRepos(user) {
    return {
        type: GET_REPOS,
            payload: {
            request: {
                url: `/users/${user}/repos`
            }
        }
    };
}