import {combineReducers} from 'redux';
import { reducer as formReducer } from 'redux-form';
import repository from "./repository.reducer"
import repositoryDetail from "./repository-detail.reducer"

const reducers = {
    repository,
    repositoryDetail,
    form: formReducer
}
const allReducers = combineReducers(reducers);
export default allReducers;