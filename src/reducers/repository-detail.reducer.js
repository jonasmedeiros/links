import { GET_REPO_INFO, GET_REPO_INFO_SUCCESS, GET_REPO_INFO_FAIL } from "../types/repository-detail.type";

const initialState = { 
    loadingInfo: false,
    errorInfo: null,
    repoInfo: {}
};

export default (state = initialState , action) => {
    switch (action.type) {
        case GET_REPO_INFO:
            return { 
                ...state, 
                loadingInfo: true,
                repoInfo: {},
                errorInfo: null
            };
        case GET_REPO_INFO_SUCCESS:
            return { 
                ...state, 
                loadingInfo: false, 
                repoInfo: action.payload.data ,
                errorInfo: null
            };
        case GET_REPO_INFO_FAIL:
            return {
                ...state,
                loadingInfo: false,
                repoInfo: {},
                errorInfo: 'Error getting repo info'
            };
        default:
            return state;
    }
}