import { GET_REPOS, GET_REPOS_SUCCESS, GET_REPOS_FAIL } from "../types/respository.type";

const initialState = { 
    loading: false,
    error: null,
    repos: [] 
};

export default (state = initialState , action) => {
    switch (action.type) {
        case GET_REPOS:
            return { 
                ...state,
                loading: true,
                repos: [],
                error: null
            };
        case GET_REPOS_SUCCESS:
            return {
                ...state, 
                loading: false, 
                repos: action.payload.data ,
                error: null
            };
        case GET_REPOS_FAIL:
          return { 
                ...state, 
                loading: false,
                repos: [],
                error: 'Error getting repos info'
            };
        default:
          return state;
    }
}