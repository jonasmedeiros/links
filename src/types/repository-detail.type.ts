export const GET_REPO_INFO = 'links/repository/INFO';
export const GET_REPO_INFO_SUCCESS = 'links/repository/INFO_SUCCESS';
export const GET_REPO_INFO_FAIL = 'links/repository/INFO_FAIL';