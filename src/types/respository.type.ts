export const GET_REPOS = 'links/repository/LOAD';
export const GET_REPOS_SUCCESS = 'links/repository/LOAD_SUCCESS';
export const GET_REPOS_FAIL = 'links/repository/LOAD_FAIL';