const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

export default values => {
  const errors = {};

  if (!values.email) {
    errors.email = "Email address must not be empty";
  } else if (!emailRegex.test(values.email)) {
    errors.email = "Invalid email address format";
  }

  return errors;
};